from fastapi.testclient import TestClient
import jwt
from src import main
from . import mockedobjects as mo


def test_all(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    client = TestClient(main.app)
    mevents = mocker.patch("src.db.models.Event.get_all")
    mevents.return_value = mo.mocked_events
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    j = resp.json()
    resp = client.get("/event/all", headers={"Authorization": f"bearer {j['session_jwt']}"})
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["status"] == "ok"
    assert j["count"] == 3
    assert j["events"] == mo.mocked_events

def test_booked_one(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    class MockedEvent:
        def jsonfy(self):
            return mo.mocked_event
            
    fake_event = MockedEvent()
    mbook = mocker.patch("src.db.models.Event.get_by_id")
    mbook.return_value = fake_event
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    session_jwt = resp.json()["session_jwt"]
    resp = client.get("/event/event_test1", headers={"Authorization": f"bearer {session_jwt}"})
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["event"] == mo.mocked_event
    assert j["status"] == "ok"

def test_delete_event_forbidden(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    class MockedEvent:
        def delete(self):
            pass
            
    fake_event = MockedEvent()
    mbook = mocker.patch("src.db.models.Event.get_by_id")
    mbook.return_value = fake_event
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    session_jwt = resp.json()["session_jwt"]
    resp = client.delete("/event/event_test1", headers={"Authorization": f"bearer {session_jwt}"})
    assert resp.status_code == 403
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["status"] == "error"
    assert j["error_description"] == "forbidden"

def test_delete_event_forbidden(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["admin1"]
    class MockedEvent:
        def delete(self):
            return True
            
    fake_event = MockedEvent()
    mbook = mocker.patch("src.db.models.Event.get_by_id")
    mbook.return_value = fake_event
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "admin1", "password": "pass1"})
    session_jwt = resp.json()["session_jwt"]
    resp = client.delete("/event/event_test1", headers={"Authorization": f"bearer {session_jwt}"})
    j = resp.json()
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    assert j["status"] == "ok"
