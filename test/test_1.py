from fastapi.testclient import TestClient

from src import main



def test_read_root():
    client = TestClient(main.app)
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"node": -1}
