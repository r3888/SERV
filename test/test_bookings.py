import requests

couchdb_base_url = f"http://admin:pass@127.0.0.1:5984"

def get_all_by_user(id):
    selector = { "username": id }
    r = query("bookings", selector)
    return r

def query(db: str, selector, limit: int = None):
    ds = {"selector": selector, "skip": 0, "execution_stats": False}
    if limit is not None:
        ds["limit"] = limit
    r = requests.post(couchdb_base_url+f"/{db}/_find", json=ds)
    return r.json().get("docs")




print(get_all_by_user("user1"))


