

pp=$(pwd)
pp=${pp%/*}
export PYTHONPATH=$pp
export GOOGLE_CLIENT_CREDS_PATH=$pp"/google_client_credentials.json"
export TWITTER_CREDS_PATH=$pp"/twitter_creds.json"

pytest