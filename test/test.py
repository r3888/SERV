import requests



base = "http://localhost:8000"

"""
def insert(db: str, doc, id_: str = None) -> str:
    if id_ is not None:
        doc["_id"] = id_
    r = requests.post("http://admin:pass@localhost:5984"+f"/{db}", json=doc)
    print(r.text)
    return r.status_code == 201
"""

def main():

    jwt = requests.post(base+"/user/login", json={'username': 'user1', 'password': 'pass1'}).json()['session_jwt']
    r = requests.get(base+'/user/me', headers={'Authorization': 'Bearer '+jwt})
    print(r.status_code, r.text)
    #r = requests.get(base+'/user/google_auth/1', headers={'Authorization': 'Bearer '+jwt})
    #print(r.status_code, r.text)
    #redurl = input("Give me the google redirect url: ")
    #r = requests.get(redurl, headers={'Authorization': 'Bearer '+jwt})
    #print(r.status_code, r.text)
    #r = requests.get(base+'/event/all', headers={'Authorization': 'Bearer '+jwt})
    #print(r.status_code, r.text)
    r = requests.get(base+'/event/all', headers={'Authorization': 'Bearer '+jwt})
    print(r.status_code, r.text)
    r = requests.post(base+'/event/event_UchjKUQf/book', headers={'Authorization': 'Bearer '+jwt})
    print(r.text, r.status_code)


if __name__ == "__main__":
    main()