

mocked_users = {'user1': {
                    'username': 'user1', 
                    'password_hashed': '$2b$12$D2072eceQkOJg8psQtoEWuAw0qtak7lsPYlcRC88ua/T4RJJ/rhIe', 
                    'email': 'user1@test.com',
                    'is_admin': False,
                    'booked_events': ["event_test1", "event_test2"]
                },
                'admin1': {
                    'username': 'admin1', 
                    'password_hashed': '$2b$12$D2072eceQkOJg8psQtoEWuAw0qtak7lsPYlcRC88ua/T4RJJ/rhIe', 
                    'email': 'admin1@test.com',
                    'is_admin': True
                }
}


mocked_events = ["event_test1", "event_test2", "event_test3"]
mocked_event = {'id': 'event_test1', 'name': 'test event', 'date': '1970-01-01', 'available': 42}
