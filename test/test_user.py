from fastapi.testclient import TestClient
import jwt
from src import main
from . import mockedobjects as mo


def test_login(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["status"] == "ok"
    d = jwt.decode(j["session_jwt"], options={"verify_signature": False})
    assert d["username"] == "user1"
    assert d["is_admin"] == False

def test_failed_login(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = None
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    assert resp.status_code == 401
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["status"] == "error"

def test_me(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    j = resp.json()
    resp = client.get("/user/me", headers={"Authorization": f"bearer {j['session_jwt']}"})
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["status"] == "ok"
    assert j["username"] == "user1" and j["email"] == "user1@test.com" and j["is_admin"] == False


def test_booked_all(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    mev = mocker.patch("src.db.models.Booking.get_all_by_user")
    mev.return_value = mo.mocked_users["user1"]["booked_events"]
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    session_jwt = resp.json()["session_jwt"]
    resp = client.get("/user/booked/all", headers={"Authorization": f"bearer {session_jwt}"})
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["bookings"] == ["event_test1","event_test2"]
    assert j["status"] == "ok"


def test_booked_one(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    class MockedBooking:
        def jsonfy(self):
            return {"username": "user1", "event_id": "event_test1", "id": "booking_test1"}
    fake_booking = MockedBooking()
    fake_booking.username = "user1"
    mbook = mocker.patch("src.db.models.Booking.get_by_id")
    mbook.return_value = fake_booking
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    session_jwt = resp.json()["session_jwt"]
    resp = client.get("/user/booked/booking_test1", headers={"Authorization": f"bearer {session_jwt}"})
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["booking"]["username"] == "user1"
    assert j["booking"]["event_id"] == "event_test1"
    assert j["booking"]["id"] == "booking_test1"
    assert j["status"] == "ok"

def test_booked_notfound(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    mbook = mocker.patch("src.db.models.Booking.get_by_id")
    mbook.return_value = None
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    session_jwt = resp.json()["session_jwt"]
    resp = client.get("/user/booked/booking_test1", headers={"Authorization": f"bearer {session_jwt}"})
    assert resp.status_code == 404
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["status"] == "error"
    assert j["error_description"] == "not found"

def test_booked_delete(mocker):
    mm = mocker.patch("src.db.connection.get_by_id")
    mm.return_value = mo.mocked_users["user1"]
    class MockedObj:
        def delete(self):
            return
        def increase_available(self):
            return
    fake_obj = MockedObj()
    fake_obj.username = "user1"
    fake_obj.event_id = "event_test1"
    mbook = mocker.patch("src.db.models.Booking.get_by_id")
    mbook.return_value = fake_obj
    mbook = mocker.patch("src.db.models.Event.get_by_id")
    mbook.return_value = fake_obj
    client = TestClient(main.app)
    resp = client.post("/user/login", json={"username": "user1", "password": "pass1"})
    session_jwt = resp.json()["session_jwt"]
    resp = client.delete("/user/booked/booking_test1", headers={"Authorization": f"bearer {session_jwt}"})
    assert resp.status_code == 200
    assert resp.headers["content-type"] == "application/json"
    j = resp.json()
    assert j["status"] == "ok"
    


