from typing import Dict, List
import bcrypt
import jwt
import datetime
from typing import Optional

from ..conf import conf
from .. import util
from . import connection as conn


class Entity:  # should be an abstract
    def save(self):
        pass

    def delete(self):
        pass
    

class User(Entity):
    id: str
    username: str
    password_hashed: str
    is_admin: bool
    email: str
    google_access_token: str = None
    google_refresh_token: str = None

    def __init__(self, user: str, email: str, is_admin: bool = False) -> None:
        self.id = user
        self.username = user
        self.email = email
        self.is_admin = is_admin
        self.google_access_token = None
        self.google_refresh_token = None

    def jsonfy(self) -> Dict:
        return {"username": self.username, "email": self.email, "is_admin": self.is_admin}

    def check_password(self, pwd: str) -> bool:
        return bcrypt.checkpw(pwd.encode(), self.password_hashed.encode())

    def set_password(self, pwd: str) -> None:
        self.password_hashed = bcrypt.hashpw(pwd.encode(), bcrypt.gensalt())
        return

    def create_jwt(self) -> str:
        expiration_ts = int(
            (datetime.datetime.now()+datetime.timedelta(hours=1)).timestamp())
        return jwt.encode({"username": self.username, "is_admin": self.is_admin, "expires": expiration_ts}, conf['sign_key'], algorithm="HS256")

    @classmethod
    def decode_jwt(cls, jwt_encoded: str) -> Optional:
        decoded = jwt.decode(
            jwt_encoded, conf['sign_key'], algorithms=["HS256"])
        expiration_date = datetime.datetime.fromtimestamp(decoded["expires"])
        if expiration_date < datetime.datetime.now():
            return None
        return decoded

    @classmethod
    def get_by_username(cls, username: str) -> Optional:
        try:
            to_ret_d = conn.get_by_id("users", username)
            to_ret = cls("", "")
            to_ret.id = to_ret_d.get("_id")
            to_ret.username = to_ret_d.get("username")
            to_ret.password_hashed = to_ret_d.get("password_hashed")
            to_ret.email = to_ret_d.get("email")
            to_ret.is_admin = to_ret_d.get("is_admin")
            to_ret.google_access_token = to_ret_d.get("google_access_token")
            to_ret.google_refresh_token = to_ret_d.get("google_refresh_token")
        except KeyError:
            return None
        except AttributeError:
            return None
        return to_ret

    def save(self):
        d = {"username": self.username,
             "password_hashed": self.password_hashed,
             "email": self.email,
             "is_admin": self.is_admin,
             "google_access_token": self.google_access_token,
             "google_refresh_token": self.google_refresh_token
             }
        r = conn.insert("users", d, self.username)
        if not r:
            return conn.update("users", self.username, d)
        return r

    def delete(self):
        return conn.delete("users", self.username)


class Event(Entity):
    id: str
    name: str
    date: str
    available: int

    def __init__(self, name: str, date: str, available: int) -> None:
        self.id = util.random_id("event")
        self.name = name
        self.date = date
        self.available = available

    def increase_available(self) -> None:
        self.available += 1
        conn.update("events", self.id, {"available": self.available})

    def decrease_available(self) -> None:
        self.available -= 1
        conn.update("events", self.id, {"available": self.available})

    @classmethod
    def get_by_id(cls, id_: str) -> Optional:
        try:
            to_ret_d = conn.get_by_id("events", id_)
            to_ret = cls("", "", 0)
            to_ret.id = to_ret_d["_id"]
            to_ret.name = to_ret_d["name"]
            to_ret.date = to_ret_d["date"]
            to_ret.available = to_ret_d["available"]
        except:
            return None
        return to_ret

    @staticmethod
    def get_all() -> List:
        r = conn.all_docs("events")
        if not r:
            return []
        return [_["id"] for _ in r]

    def save(self) -> bool:
        d = {
            "name": self.name,
            "date": self.date,
            "available": self.available
        }
        r = conn.insert("events", d, self.id)
        if not r:
            return conn.update("events", self.id, d)
        return r

    def delete(self) -> bool:
        return conn.delete("events", self.id)

    def jsonfy(self) -> Dict:
        return {"id": self.id, "name": self.name, "date": self.date, "available": self.available}


class Booking(Entity):
    id: str
    username: str
    event_id: str

    def __init__(self, user: User, event: Event) -> None:
        self.id = util.random_id("booking")
        self.username = user.username if user is not None else ''
        self.event_id = event.id if user is not None else ''

    def jsonfy(self):
        return {"id": self.id, "username": self.username, "event_id": self.event_id}

    @classmethod
    def get_by_id(cls, id_: str) -> Optional:
        try:
            to_ret_d = conn.get_by_id("bookings", id_)
            to_ret = cls(None, None)
            to_ret.id = to_ret_d["_id"]
            to_ret.username = to_ret_d["username"]
            to_ret.event_id = to_ret_d["event_id"]
        except Exception as e:
            print('EXCEPTION: '+str(e))
            return None
        return to_ret

    @staticmethod
    def get_all_by_user(user: User) -> List:
        selector = { "username": user.id }
        r = conn.query("bookings", selector)
        r = list(map(lambda y: {"_id": y["_id"], "username": y["username"], "event_id": y["event_id"]}, r))
        return r

    def save(self):
        d = {
            "username": self.username,
            "event_id": self.event_id
        }
        r = conn.insert("bookings", d, self.id)
        if not r:
            return conn.update("bookings", self.id, d)
        return r

    def delete(self):
        return conn.delete("bookings", self.id)


'''
mocked_users = {'user1': {
                    'username': 'user1', 
                    'password_hashed': '$2b$12$D2072eceQkOJg8psQtoEWuAw0qtak7lsPYlcRC88ua/T4RJJ/rhIe', 
                    'email': 'user1@test.com',
                    'is_admin': False
                },
                'admin1': {
                    'username': 'admin1', 
                    'password_hashed': '$2b$12$D2072eceQkOJg8psQtoEWuAw0qtak7lsPYlcRC88ua/T4RJJ/rhIe', 
                    'email': 'admin1@test.com',
                    'is_admin': True
                }
}
'''
