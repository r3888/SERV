from dataclasses import field
from typing import Dict, List
import requests
from ..conf import conf

host, port, buser, bpass = conf['couchdb_host'], conf['couchdb_port'], conf['couchdb_user'], conf['couchdb_pass']

couchdb_base_url = f"http://{buser}:{bpass}@{host}:{port}"


def check_connection() -> bool:
    try:
        r = requests.get(couchdb_base_url+"/_up", timeout=1)
        return r.status_code == 200
    except:
        return False


def insert(db: str, doc: Dict, id_: str = None) -> str:
    if id_ is not None:
        doc["_id"] = id_
    r = requests.post(couchdb_base_url+f"/{db}", json=doc)
    return r.status_code == 201


def update(db: str, id_: str, fields: Dict) -> bool:
    doc = get_by_id(db, id_)
    if doc is None:
        return False
    rev = doc.pop("_rev")
    for k, v in fields.items():
        doc[k] = v
    r = requests.put(couchdb_base_url+f"/{db}/{id_}?rev={rev}", json=doc)
    return r.status_code == 201


def delete(db: str, id_: str) -> bool:
    doc = get_by_id(db, id_)
    if doc is None:
        return False
    rev = doc["_rev"]
    r = requests.delete(couchdb_base_url+f"/{db}/{id_}?rev={rev}")
    return r.status_code == 200


def get_by_id(db: str, id_: str) -> Dict:
    r = requests.get(couchdb_base_url+f"/{db}/{id_}")
    if r.status_code == 404:
        return None
    return r.json()


def query(db: str, selector: Dict, limit: int = None) -> List:
    ds = {"selector": selector, "skip": 0, "execution_stats": False}
    if limit is not None:
        ds["limit"] = limit
    r = requests.post(couchdb_base_url+f"/{db}/_find", json=ds)
    return r.json().get("docs")


def all_docs(db: str) -> List:
    r = requests.get(couchdb_base_url+f"/{db}/_all_docs")
    return r.json().get("rows")
