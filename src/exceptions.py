
from typing import Dict
from . import util


class AppException(Exception):
    def __init__(self, status_code: int, detail: str):
        super().__init__(str(status_code)+' '+detail)
        self.status_code = status_code
        self.content = util.error(detail)


class NoAuthException(AppException):
    def __init__(self):
        super().__init__(401, "unauthorized")


class ForbiddenException(AppException):
    def __init__(self):
        super().__init__(403, "forbidden")


class NotFoundException(AppException):
    def __init__(self):
        super().__init__(404, "not found")