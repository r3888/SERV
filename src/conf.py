
import json
import os


conf = {}

conf["rabbitmq_host"] = os.getenv("RABBITMQ_HOST", "localhost")
conf["rabbitmq_port"] = os.getenv("RABBITMQ_PORT", "5672") #5672 default


conf["sign_key"] = os.environ.get("SIGN_KEY", "secret")
conf["couchdb_host"] = os.environ.get("COUCHDB_HOST", "localhost")
conf["couchdb_port"] = os.environ.get("COUCHDB_PORT", "5984")
conf["couchdb_user"] = os.environ.get("COUCHDB_USER", "admin")
conf["couchdb_pass"] = os.environ.get("COUCHDB_PASS", "pass")

conf["google_client_creds_path"] = os.environ.get("GOOGLE_CLIENT_CREDS_PATH", "google_client_credentials.json")
conf["twitter_creds_path"] = os.environ.get("TWITTER_CREDS_PATH", "twitter_creds.json") 

conf["twitter"] = json.load(open(conf["twitter_creds_path"], "r"))
