from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
import os


from .routes import user, admin, event
from .util import error
from . import exceptions
from . import conf


app = FastAPI()


origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=False,  # cause we don't want CSRF
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/')
async def index():
    return {'node': os.getenv('NODE', -1)}


@app.exception_handler(Exception)
async def base_exception_handler(request: Request, exc: Exception):
    return JSONResponse(status_code=500, content=error("generic error"))


@app.exception_handler(exceptions.AppException)
async def app_exception_handler(request: Request, exc: exceptions.AppException):
    return JSONResponse(status_code=exc.status_code, content=exc.content)


app.include_router(user.router)
app.include_router(admin.router)
app.include_router(event.router)
