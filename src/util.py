from functools import wraps
import pika
import secrets
import string
from json import dumps as json_dumps
from typing import Callable, Dict
from fastapi import Request

from .db import models
from .exceptions import NoAuthException, ForbiddenException
from .conf import conf


def error(s: str) -> Dict:
    return {"status": "error", "error_description": s}


def ok(d: Dict = {}) -> Dict:
    dc = d.copy()
    dc["status"] = "ok"
    return dc


def random_id(prefix: str) -> str:
    b = secrets.token_bytes(8)
    letters = string.ascii_lowercase+string.ascii_uppercase+string.digits
    l = len(letters)
    return f"{prefix}_{''.join([letters[i%l] for i in b])}"


def auth_required(f: Callable) -> Callable:  # who let the dogs out
    @wraps(f)
    async def auth_wrapper(*args, request: Request, username: str = None, **kwargs):
        try:
            auth_header = request.headers["Authorization"].split(' ')
            assert len(auth_header) == 2 and auth_header[0].lower() == "bearer"
            session_jwt = auth_header[1]
            user = models.User.decode_jwt(session_jwt)
            assert user is not None
        except (KeyError, AssertionError):
            raise NoAuthException()
        else:
            print(args, kwargs)
            return await f(*args, request=request, username=user["username"], **kwargs)
    return auth_wrapper


def auth_cookie_required(f: Callable) -> Callable:  # who let the dogs out
    @wraps(f)
    async def auth_wrapper(*args, request: Request, username: str = None, **kwargs):
        try:
            session_jwt = request.cookies.get('jwt')
            assert session_jwt is not None
            user = models.User.decode_jwt(session_jwt)
            assert user is not None
        except (KeyError, AssertionError):
            raise NoAuthException()
        else:
            print(args, kwargs)
            return await f(*args, request=request, username=user["username"], **kwargs)
    return auth_wrapper


def admin_auth_required(f: Callable) -> Callable:  # who let the dogs out
    @wraps(f)
    async def auth_wrapper(*args, request: Request, username: str = None, **kwargs):
        try:
            auth_header = request.headers["Authorization"].split(' ')
            assert len(auth_header) == 2 and auth_header[0].lower() == "bearer"
            session_jwt = auth_header[1]
            user = models.User.decode_jwt(session_jwt)
            assert user is not None
        except (KeyError, AssertionError):
            raise NoAuthException()
        else:
            if user["is_admin"] is False:
                raise ForbiddenException()
            print(args, kwargs)
            return await f(*args, request=request, username=user["username"], **kwargs)
    return auth_wrapper


def send_to_queue(queue_name: str, d: Dict):
    host = conf["rabbitmq_host"]
    port = conf["rabbitmq_port"]
    connection = pika.BlockingConnection(
        pika.URLParameters(f"amqp://{host}:{port}"))
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    message = json_dumps(d)
    channel.basic_publish(exchange='',
                          routing_key=queue_name,
                          body=message)
    connection.close()
