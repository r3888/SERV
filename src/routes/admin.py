from fastapi import APIRouter, HTTPException, Request, Path
from pydantic import BaseModel

from .. import util, exceptions
from ..db import models


router = APIRouter(
    prefix="/admin",
    tags=["admin"],
    #dependencies=[Depends(get_token_header)],
    responses={404: util.error("not found")},
)

'''

@route.get('/')
async def get_booked_event(booking_id: int = Path(), request: Request, username: str = None):
    user = models.User.get_by_username(username)


@router.get('/register')
async def get_me():
    raise HTTPException(status_code=501, error('not implemented'))
'''





