from fastapi import APIRouter, Request, Path, Response
from fastapi.responses import RedirectResponse
from pydantic import BaseModel, validator
from google_auth_oauthlib.flow import Flow
from urllib.parse import urlparse


from .. import util, exceptions
from ..db import models
from ..conf import conf


class Creds(BaseModel):
    username: str
    password: str


class UserPostModel(BaseModel):
    username: str
    password: str
    password_repeat: str
    is_admin: bool
    email: str

    @validator("email")
    def validate_email(cls, v):
        return v


router = APIRouter(
    prefix="/user",
    tags=["user"],
    # dependencies=[Depends(get_token_header)],
    responses={404: util.error("not found")},
)


@router.get('/me')
@util.auth_required
async def get_me(request: Request, username: str = None):
    user = models.User.get_by_username(username)
    return util.ok(user.jsonfy())


@router.post('/login')
async def login(creds: Creds, response: Response = None):
    user = models.User.get_by_username(creds.username)
    if not (user and user.check_password(creds.password)):
        raise exceptions.AppException(401, "wrong credentials")
    else:
        session_jwt = user.create_jwt()
        response.set_cookie(
            key="jwt",
            value=session_jwt,
            httponly=False,
            secure=True,
            samesite='None',
        )
        return util.ok({'session_jwt': session_jwt})


@router.post('/register', status_code=201)
@util.admin_auth_required
async def register(user_data: UserPostModel, request: Request, username: str = None):
    try:
        user = models.User(user_data.username,
                           user_data.email, user_data.is_admin)
        assert user_data.password == user_data.password_repeat
        user.set_password(user_data.password)
        user.save()
    except AssertionError:
        return exceptions.AppException(422, "password and password_repeat do not match")
    return util.ok({"message": "created", "user": user.jsonfy()})


@router.get('/google_auth/1')
@router.post('/google_auth/1')
@util.auth_required
async def google_auth(request: Request = None, username: str = None, response: Response = None):
    flow = Flow.from_client_secrets_file(conf["google_client_creds_path"],
                                         scopes=["https://www.googleapis.com/auth/calendar"])

    referer = request.headers["referer"]
    response.set_cookie(
            key="last_referer",
            value=referer,
            httponly=False,
            secure=True,
            samesite='None',
        )
    parsed = urlparse(referer)
    origin = parsed.scheme+"://"+parsed.netloc
    flow.redirect_uri = origin+"/api/user/google_auth/2"
    auth_url, _ = flow.authorization_url(prompt='consent')

    return util.ok({'go_to': auth_url})


@router.get('/google_auth/2')
@util.auth_cookie_required
async def google_auth(code: str, request: Request = None, username: str = None, response: Response = None):
    try:
        flow = Flow.from_client_secrets_file(conf["google_client_creds_path"],
                                             scopes=["https://www.googleapis.com/auth/calendar"])
        #flow.redirect_uri = str(request.headers.get('origin', 'http://localhost:3000'))+"/?status=success"
        referer = request.cookies.get("last_referer")
        if referer == "deleted": raise Exception
        parsed = urlparse(referer)
        origin = parsed.scheme+"://"+parsed.netloc
        flow.redirect_uri = origin+"/api/user/google_auth/2"

        auth_url, _ = flow.authorization_url(prompt='consent')
        flow.fetch_token(code=code)
        user = models.User.get_by_username(username)
        user.google_access_token = flow.credentials.token
        user.google_refresh_token = flow.credentials.refresh_token
        user.save()
        response.set_cookie(
            key="last_referer",
            value="deleted",
            httponly=False,
            secure=True,
            samesite='None',
        )
    except Exception as e:
        raise exceptions.AppException(400, str(e))
    return RedirectResponse(str(request.base_url) + '?status=success')
    # return util.ok({})


@router.get('/booked/all')
@util.auth_required
# si potrebbe pensare di inserire la paginazione
async def get_booked_all(request: Request, username: str = None):
    user = models.User.get_by_username(username)
    return util.ok({"bookings": models.Booking.get_all_by_user(user)})


@router.get('/booked/{booking_id}')
@util.auth_required
async def get_booked_event(booking_id: str = Path(), request: Request = None, username: str = None):
    user = models.User.get_by_username(username)
    try:
        booking = models.Booking.get_by_id(booking_id)
        if booking is None:
            raise exceptions.NotFoundException()
        assert booking.username == username
    except AssertionError as e:
        raise exceptions.ForbiddenException
    return util.ok({"booking": booking.jsonfy()})


@router.delete('/booked/{booking_id}')
@util.auth_required
async def delete_booked_event(booking_id: str = Path(), request: Request = None, username: str = None):
    try:
        booking = models.Booking.get_by_id(booking_id)
        if booking is None:
            raise exceptions.NotFoundException()
        assert booking.username == username
        event = models.Event.get_by_id(booking.event_id)
        booking.delete()
        event.increase_available()
    except AssertionError as e:
        raise exceptions.ForbiddenException
    return util.ok({})
