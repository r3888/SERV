import re
from requests_oauthlib import OAuth1Session
from fastapi import APIRouter, HTTPException, Request, Path
from fastapi.responses import RedirectResponse
from pydantic import BaseModel, Field, validator
from google_auth_oauthlib.flow import Flow
import requests

from .. import util, exceptions
from ..db import models, connection as conn
from ..conf import conf


class EventPostModel(BaseModel):
    available: int = Field(
        gt=0, description="The available places must be greater than 0")
    name: str
    date: str

    @validator("date")
    def validate_date(cls, d):
        if not re.match(r"^[1-9][0-9]{3}-((0[1-9])|(1[0-2]))-(([0-2][0-9])|(3[0,1]))$", d):
            raise ValueError("date must be in yyyy-mm-dd format")
        return d


router = APIRouter(
    prefix="/event",
    tags=["event"],
    # dependencies=[Depends(get_token_header)],
    responses={404: util.error("not found")},
)


@router.get('/all')
@util.auth_required
async def get_events_all(request: Request, username: str = None):
    events = models.Event.get_all()
    return util.ok({'events': events, "count": len(events)})


@router.post('/new', status_code=201)
@util.admin_auth_required
async def create_event(event_post: EventPostModel, request: Request, username: str = None):
    try:
        nm = models.Event(event_post.name, event_post.date,
                          event_post.available)
        assert nm.save() is True

        oauth = OAuth1Session(conf["twitter"]["API_KEY"], client_secret=conf["twitter"]["API_KEY_SECRET"],
                              resource_owner_key=conf["twitter"]["ACCESS_TOKEN"],
                              resource_owner_secret=conf["twitter"]["ACCESS_TOKEN_SECRET"])

        tweet = f"Event \"{nm.name}\" for the date {nm.date} has been just created!\n"\
                "Go to our website and book your reservation!"
        res = oauth.post("https://api.twitter.com/2/tweets",
                         json={"text": tweet})


        user_emails = list(map(lambda y: y["email"], conn.query("users", {"is_admin": False})))
        util.send_to_queue("bookingn_bulk_email", {
                           "email_text": tweet,
                           'email_addresses': user_emails})  # no blocking

        return util.ok({"event": nm.jsonfy()})
    except ValueError:
        raise exceptions.AppException(400, "bad request")


@router.get('/{event_id}')
@util.auth_required
async def get_event(event_id: str = Path(), request: Request = None, username: str = None):
    event = models.Event.get_by_id(event_id)
    if event is None:
        return exceptions.NotFoundException
    d = {'event': event.jsonfy()}
    return util.ok(d)


@router.post('/{event_id}/book')
@util.auth_required
async def book_event(event_id: str = Path(), request: Request = None, username: str = None):
    user = models.User.get_by_username(username)
    print(request.base_url)
    if user.google_access_token is None and user.google_refresh_token is None:
        return RedirectResponse(str(request.base_url)+"user/google_auth/1")
    event = models.Event.get_by_id(event_id)
    if event is None:
        raise exceptions.NotFoundException
    booking = models.Booking(user, event)
    if not booking.save():
        return exceptions.AppException(400, "not able to book this event")
    event.decrease_available()
    d = {
        "summary": event.name,
        "description": event.name,
        "start": {"date": event.date},
        "end": {"date": event.date},
    }
    resp = requests.post("https://www.googleapis.com/calendar/v3/calendars/primary/events",
                         json=d,
                         headers={"Authorization": f"Bearer {user.google_access_token}"})
    # check su refresh necessario qui, per ora
    if resp.status_code == 200:
        return util.ok({"message": "booked and event created on your g-calendar"})
    user.google_access_token = None
    user.google_refresh_token = None
    user.save()
    return util.ok({"message": "booked"})


@router.delete('/{event_id}')
@util.admin_auth_required
async def delete_event(event_id: str = Path(), request: Request = None, username: str = None):
    event = models.Event.get_by_id(event_id)
    
    if event and event.delete():
        return util.ok()
    else:
        raise exceptions.AppException(400, "event not deleted")


@router.put('/{event_id}')
@util.admin_auth_required
async def modify_event(event_post: EventPostModel, event_id: str = Path(), request: Request = None, username: str = None):
    try:
        m = models.Event.get_by_id(event_id)
        assert m is not None
        m.name = event_post.name
        m.data = event_post.date
        m.available = event_post.available
        assert m.save() is True
    except ValueError:
        raise exceptions.AppException(400, "bad request")
    except AssertionError:
        raise exceptions.NotFoundException()


'''
@router.get('/register')
async def get_me():
    raise HTTPException(status_code=501, error('not implemented'))
'''
